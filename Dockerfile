FROM scratch

COPY ./blinkt .

EXPOSE 80
CMD ["./app/blinkt"]

