module gitlab.com/baroprime/led-app/device-blinkt

go 1.12

require (
	github.com/alexellis/blinkt_go v0.0.0-20180120180744-cc0ca163e0bc
	github.com/gorilla/mux v1.7.2
)
