package main

import (
	"log"
	"net/http"
	"io/ioutil"
	"encoding/json"
	"strconv"
	"os"
	"math/rand"
	"fmt"
	"strings"


	"github.com/gorilla/mux"
	. "github.com/alexellis/blinkt_go/sysfs"
)

var bl = NewBlinkt()

//InitDevice initializes the device
func InitDevice(){
	if _, err := os.Stat("/path/to/whatever"); err == nil {
		log.Println("File exists")
	} else if os.IsNotExist(err) {
		client := &http.Client{}
		//create new ID between 10000000 and 99999999
		newID := randInt(10000000, 99999999)
		newName := fmt.Sprintf("device%d",newID)
		dev := NewDevice(fmt.Sprint(newID),newName,"blinkt","0","0","0","0")

		//convert device struct to JSON
		devJSON, err := json.Marshal(dev)
		if err != nil {
			log.Println("Error converting struct to JSON", err)
		}

		f, err := os.Create("./device.json")
		if err != nil{
			log.Println("Error creating file")
		}
		defer f.Close()

		err = ioutil.WriteFile("./device.json", devJSON, 0644)
		if err != nil {
			log.Println("Error writing to file", err)
		}

		serviceName := "redis-rest:80"
		updateURL := fmt.Sprintf("http://%s/api/create-device", serviceName)
		//create request to agent
		req, err := http.NewRequest("POST", updateURL, strings.NewReader(string(devJSON)))
		if err != nil {
			log.Println("Error creating request")
			return
		}
		//send request
		_, err = client.Do(req)
		if err != nil {
			log.Println("Error connection to Redis-REST", err)
			return
		}
	
	}
}

//InitBlinkt ...
func InitBlinkt() {
	bl.Setup()
	bl.SetClearOnExit(true)
}

func updateRGB(w http.ResponseWriter, r *http.Request){

	dev := NewEmptyDevice()
	
	//read device info from client
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println("Error parsing client data", err)
	}
	err = json.Unmarshal(body, &dev)
	if err != nil {
		log.Println("Error parsing client data", err)
		return
	}

	ID ,err := strconv.Atoi(dev.ID)
	if err != nil{
		log.Println("Cannot convert R to int")
	}

	if ID == getID(){
		bl.SetAll(convertToInt(dev)).SetBrightness(convertToFloat(dev))
		bl.Show()
	}
}

func convertToInt(dev *Device)(int, int, int ){
	red ,err := strconv.Atoi(dev.RGBA.R)
	if err != nil{
		log.Println("Cannot convert R to int")
	}
	green ,err := strconv.Atoi(dev.RGBA.G)
	if err != nil{
		log.Println("Cannot convert G to int")
	}
	blue ,err := strconv.Atoi(dev.RGBA.B)
	if err != nil{
		log.Println("Cannot convert B to int")
	}
	return red,green,blue
}

func convertToFloat(dev *Device) float64{
	alpha ,err := strconv.ParseFloat(dev.RGBA.A,64)
	if err != nil{
		log.Println("Cannot convert A to float64")
	}
	return alpha
}

func randInt(min int, max int) int {
    return min + rand.Intn(max-min)
}

func getID() int{
	dev := NewEmptyDevice()
	
	//read device info from client
	file, err := ioutil.ReadFile("./device.json")
	if err != nil {
		log.Println("Error parsing client data", err)
	}
	err = json.Unmarshal(file, &dev)
	if err != nil {
		log.Println("Error parsing client data", err)
	}
	ID ,err := strconv.Atoi(dev.ID)
	if err != nil{
		log.Println("Cannot convert R to int")
	}

	return ID
}

func main() {
	InitDevice()
	InitBlinkt()
	r := mux.NewRouter()
	r.HandleFunc("/update-RGB", updateRGB).Methods("PUT")
	log.Fatal(http.ListenAndServe(":80", r))
}
